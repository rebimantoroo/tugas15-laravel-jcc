@extends('adminlte.master')

@section('judulFile')
  Dashboard
@endsection

@section('judul')
  Selamat Datang di Halaman Dashboard
@endsection


@section('isi')
    <h3 align="center"> Silahkan Pilih Table</h3>
    <div class="row">
      <div class="col-6">
        <div class="small-box bg-info">
          <div class="inner">
            <h3>Table</h3>
            <p></p>
          </div>
          <div class="icon">
            <i class="ion ion-bag"></i>
          </div>
          <a href="/cast" class="small-box-footer">Menuju halaman Table Cast <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <div class="col-6">
        <div class="small-box bg-success">
          <div class="inner">
            <h3>Tambah Cast</h3>
            <p></p>
          </div>
          <div class="icon">
            <i class="ion ion-bag"></i>
          </div>
          <a href="/cast/create" class="small-box-footer">Menuju halaman Tambah Cast<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>

    </div>

@endsection
