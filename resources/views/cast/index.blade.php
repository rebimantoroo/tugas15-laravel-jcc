@extends('adminlte.master')

@section('isi')
    <div class="mt-3 ml-3">
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Tabel Cast Film</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(session('berhasil'))
                    <div class="alert alert-success">
                        {{session('berhasil')}}
                    </div>
                @endif
                <a class="btn btn-primary mb-sm-2" href="/cast/create">Tambah Cast Baru</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Nama</th>
                      <th>Umur</th>
                      <th>Bio</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($cast as $key => $cast)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$cast->nama}}</td>
                            <td>{{$cast->umur}}</td>
                            <td>{{$cast->bio}}</td>
                            <td style="display:flex">
                                <a href="/cast/{{$cast->id}}" class="btn btn-info btn-sm">show</a>
                                <a href="/cast/{{$cast->id}}/edit" class="btn btn-default btn-sm">edit</a>
                                <form action="/cast/{{$cast->id}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                    @empty 
                        <td colspan="5" align="center">Data Kosong</td>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
    </div>
@endsection